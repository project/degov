# $Id$
#
# German translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  degov_media_instagram.info.yml: n/a
#  templates/media--instagram--usage.html.twig: n/a
#  config/install/field.field.media.instagram.field_include_search.yml: n/a
#  config/install/field.field.media.instagram.field_tags.yml: n/a
#  config/install/field.field.media.instagram.field_title.yml: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2019-11-19 12:58+0100\n"
"PO-Revision-Date: 2019-12-10 12:57+0100\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: German <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: degov_media_instagram.info.yml:0
msgid "deGov - Media instagram"
msgstr "deGov - Medien Instagram"

#: degov_media_instagram.info.yml:0
msgid "This module extends the media type instagram."
msgstr "Dieses Modul erweitert den Medientyp Instagram."

#: degov_media_instagram.info.yml:0
msgid "deGov"
msgstr "deGov"

#: templates/media--instagram--usage.html.twig:26
msgid "Usage"
msgstr "Verwendung"

#: config/install/field.field.media.instagram.field_include_search.yml:0
msgid "Mediathek"
msgstr "Mediathek"

#: config/install/field.field.media.instagram.field_include_search.yml:0
msgid "An"
msgstr "An"

#: config/install/field.field.media.instagram.field_include_search.yml:0
msgid "Aus"
msgstr "Aus"

#: config/install/field.field.media.instagram.field_tags.yml:0
msgid "Schlagwörter"
msgstr "Schlagwörter"

#: config/install/field.field.media.instagram.field_title.yml:0
msgid "Öffentlicher Titel"
msgstr "Öffentlicher Titel"

