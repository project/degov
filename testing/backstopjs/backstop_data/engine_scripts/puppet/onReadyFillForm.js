/**
 * @file
 */

module.exports = async(page, scenario, vp) => {

  // Allows form population and focus.
  // Only one element can have focus.
  //
  // Example:
  //
  // "fillForm": [
  //   {
  //     "id": "edit-name",   <---required
  //     "text": "incredibleuser@my.nrw"
  //   },
  //   {
  //     "id": "edit-pass",  <---required
  //     "text": "",
  //     "focus": true
  //   }
  // ],
  if (scenario.fillForm && Array.isArray(scenario.fillForm)) {
    await page.waitForSelector(`#${scenario.fillForm[0].id}`);
    await page.evaluate(fillForm => {
      fillForm.forEach((data, index) => {
        const elm = document.getElementById(data.id);
        if (!data.id) {
          throw `Missing id scenario.fillForm[${index}]`
        }
        if (data.text) {
          elm.value = data.text;
        }
        if (data.placeholder) {
          elm.placeholder = data.placeholder;
        }
        if (data.focus) {
          elm.focus();
        }
      });
    }, scenario.fillForm);
    await page.waitForTimeout(500);
  }
}
