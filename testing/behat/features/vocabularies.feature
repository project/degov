@api @drupal @access
Feature: CRUD vocabularies

  Scenario: As a editor I can access the taxonomy overview
    Given I am logged in as a user with the "editor" role
    And I am at "admin/structure/taxonomy"
    Then I should see "Taxonomien dienen zur Kategorisierung von Inhalten"

  Scenario: As a manager I can access the taxonomy overview
    Given I am logged in as a user with the "manager" role
    And I am at "admin/structure/taxonomy"
    Then I should see "Taxonomien dienen zur Kategorisierung von Inhalten"

  Scenario: As a editor I can create and edit terms of vocabulary topic
    Given I am logged in as a user with the "editor" role
    And I am at "admin/structure/taxonomy/manage/topic/add"
    And I fill in "Name" with "Topic 1"
    And I scroll to bottom
    And I press button with label "Save" via translated text
    Then I should see "Der neue Begriff Topic 1 wurde erstellt."
    When I open taxonomy term "edit" url by name "Topic 1"
    Then I should see "Begriff bearbeiten"
    When I open taxonomy term "delete" url by name "Topic 1"
    Then I should see "Zugriff verweigert"

  Scenario: As a manager I can create, edit and delete terms of vocabulary topic
    Given I am logged in as a user with the "manager" role
    And I am at "admin/structure/taxonomy/manage/topic/add"
    And I fill in "Name" with "Topic 2"
    And I scroll to bottom
    And I press button with label "Save" via translated text
    Then I should see "Der neue Begriff Topic 2 wurde erstellt."
    When I open taxonomy term "edit" url by name "Topic 1"
    Then I should see "Begriff bearbeiten"
    When I open taxonomy term "delete" url by name "Topic 1"
    And I press button with label "Delete" via translated text
    Then I should see "Der Begriff Topic 1 wurde gelöscht."
    When I open taxonomy term "delete" url by name "Topic 2"
    And I press button with label "Delete" via translated text
    Then I should see "Der Begriff Topic 2 wurde gelöscht."

  Scenario: As a editor I can create, edit and delete terms of vocabulary section
    Given I am logged in as a user with the "editor" role
    And I am at "admin/structure/taxonomy/manage/section/add"
    And I fill in "Name" with "Section 1"
    And I scroll to bottom
    And I press button with label "Save" via translated text
    Then I should see "Der neue Begriff Section 1 wurde erstellt."
    When I open taxonomy term "edit" url by name "Section 1"
    Then I should see "Begriff bearbeiten"
    When I open taxonomy term "delete" url by name "Section 1"
    And I press button with label "Delete" via translated text
    Then I should see "Der Begriff Section 1 wurde gelöscht."

  Scenario: As a manager I can create, edit and delete terms of vocabulary section
    Given I am logged in as a user with the "manager" role
    And I am at "admin/structure/taxonomy/manage/section/add"
    And I fill in "Name" with "Section 1"
    And I scroll to bottom
    And I press button with label "Save" via translated text
    Then I should see "Der neue Begriff Section 1 wurde erstellt."
    When I open taxonomy term "edit" url by name "Section 1"
    Then I should see "Begriff bearbeiten"
    When I open taxonomy term "delete" url by name "Section 1"
    And I press button with label "Delete" via translated text
    Then I should see "Der Begriff Section 1 wurde gelöscht."

  Scenario: As a editor I can create terms of vocabulary copyright
    Given I am logged in as a user with the "editor" role
    And I am at "admin/structure/taxonomy/manage/copyright/add"
    And I fill in "Name" with "Copyright 1"
    And I scroll to bottom
    And I press button with label "Save" via translated text
    Then I should see "Der neue Begriff Copyright 1 wurde erstellt."
    When I open taxonomy term "edit" url by name "Copyright 1"
    Then I should see "Zugriff verweigert"
    When I open taxonomy term "delete" url by name "Copyright 1"
    Then I should see "Zugriff verweigert"

  Scenario: As a manager I can create, edit and delete terms of vocabulary copyright
    Given I am logged in as a user with the "manager" role
    And I am at "admin/structure/taxonomy/manage/copyright/add"
    And I fill in "Name" with "Copyright 2"
    And I scroll to bottom
    And I press button with label "Save" via translated text
    Then I should see "Der neue Begriff Copyright 2 wurde erstellt."
    When I open taxonomy term "edit" url by name "Copyright 2"
    Then I should see "Begriff bearbeiten"
    When I open taxonomy term "delete" url by name "Copyright 1"
    And I press button with label "Delete" via translated text
    Then I should see "Der Begriff Copyright 1 wurde gelöscht."
    When I open taxonomy term "delete" url by name "Copyright 2"
    And I press button with label "Delete" via translated text
    Then I should see "Der Begriff Copyright 2 wurde gelöscht."

  Scenario: As a manager I can create, edit and delete terms of vocabulary tags
    Given I am logged in as a user with the "manager" role
    And I am at "admin/structure/taxonomy/manage/tags/add"
    And I fill in "Name" with "Tag 1"
    And I scroll to bottom
    And I press button with label "Save" via translated text
    Then I should see "Der neue Begriff Tag 1 wurde erstellt."
    When I open taxonomy term "edit" url by name "Tag 1"
    Then I should see "Begriff bearbeiten"
    When I open taxonomy term "delete" url by name "Tag 1"
    And I press button with label "Delete" via translated text
    Then I should see "Der Begriff Tag 1 wurde gelöscht."

  Scenario: As a manager I can create, edit and delete terms of vocabulary synonyms
    Given I am logged in as a user with the "manager" role
    And I am at "admin/structure/taxonomy/manage/synonyms/add"
    And I fill in "Name" with "synonyms 1"
    And I scroll to bottom
    And I press button with label "Save" via translated text
    Then I should see "Der neue Begriff synonyms 1 wurde erstellt."
    When I open taxonomy term "edit" url by name "synonyms 1"
    Then I should see "Begriff bearbeiten"
    When I open taxonomy term "delete" url by name "synonyms 1"
    And I press button with label "Delete" via translated text
    Then I should see "Der Begriff synonyms 1 wurde gelöscht."
