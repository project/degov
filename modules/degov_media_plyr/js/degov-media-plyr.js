(function ($, Drupal, drupalSettings) {
  'use strict';
  /* global Plyr */

  Drupal.degovPlyrs = [];
  Drupal.behaviors.degovMediaPlyr = {
    attach: function (context) {
      if (typeof Plyr != 'undefined') {
        $(context).find('.degov-media-plyr').each(function () {
          if ($.inArray(this.id, Drupal.degovPlyrs) === -1) {
            Drupal.degovPlyrs[this.id] = new Plyr(Drupal.behaviors.degovMediaPlyr.getContainer(this), {
              title: this.title ? this.title : $(this).parents('.video__video').attr('title'),
              iconUrl: '/modules/contrib/video_embed_field_plyr/assets/plyr/plyr.svg',
              i18n: drupalSettings.degovMediaPlyr.i18n,
            });
            // Play button should focus on plyr focus, not twice.
            Drupal.degovPlyrs[this.id].on('ready', function (event) {
              $(event.target)
                .find('.plyr__control--overlaid')
                .attr('tabindex', '-1');
            });
            Drupal.degovPlyrs[this.id].on('playing', function (event) {
             var slickSlideshow = $(event.target).parents('.slick-slider.slick-initialized');
             if (slickSlideshow) {
               slickSlideshow.parent().find('.slick__pause').trigger('click');
             }
            });

            // Hide Slider type 2 text when playing.
            var type2wrapper = $(this).parents('.slideshow-type-2');
            if (type2wrapper.length) {
              Drupal.degovPlyrs[this.id].on('playing', function (event) {
                type2wrapper.addClass('is-playing')
              });
              Drupal.degovPlyrs[this.id].on('pause', function (event) {
                type2wrapper.removeClass('is-playing')
              });
            }
          }
        });
      }
    },

    /**
     *
     * @param degovMediPlyr
     *   Element with .degov-media-plyr class.
     * @return jQuery
     *   Container of plyr.
     */
    getContainer: function(degovPlyr) {
      const elm = $(degovPlyr)
      return elm.hasClass('degov-media-plyr__embed') ? elm.children('div [data-plyr-provider]') : elm;
    }
  };
}(jQuery, Drupal, drupalSettings));
