<?php

/**
 * @file
 * Handles Plyr integration for all video and audio types.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_library_info_alter().
 */
function degov_media_plyr_library_info_alter(&$libraries, $extension) {
  // Unset video_embed_field_plyr defult JS to prevent
  // site-builders per field configuration for only embeded videos.
  if ($extension === 'video_embed_field_plyr') {
    if (isset($libraries['video_embed_field_plyr.drupal']['js']['assets/drupal.video-embed-field-plyr.js'])) {
      unset($libraries['video_embed_field_plyr.drupal']['js']['assets/drupal.video-embed-field-plyr.js']);
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK().
 *
 * Custom template to implement deGov common default settings in code.
 */
function degov_media_plyr_theme_suggestions_video_embed_plyr(array $variables) {
  return [
    'degov_media_plyr__video_embed_plyr',
  ];
}

/**
 * Implements hook_theme().
 */
function degov_media_plyr_theme($existing, $type, $theme, $path) {
  return [
    'degov_media_plyr__video_embed_plyr' => [
      'base hook' => 'video_embed_plyr',
      'template' => 'degov-media-plyr--video-embed-plyr',
    ],
  ];
}

/**
 * Attach required library and settings.
 */
function degov_media_plyr_attach_to_render_array(&$variables) {
  $variables['#attached']['library'][] = 'degov_media_plyr/degov_media_plyr.default';
  $variables['#attached']['drupalSettings']['degovMediaPlyr']['i18n'] = [
    'restart' => t('Restart'),
    'rewind' => t('Rewind {seektime}s'),
    'play' => t('Play'),
    'pause' => t('Pause'),
    'fastForward' => t('Forward {seektime}s'),
    'seek' => t('Seek'),
    'seekLabel' => t('{currentTime} of {duration}'),
    'played' => t('Played'),
    'buffered' => t('Buffered'),
    'currentTime' => t('Current time'),
    'duration' => t('Duration'),
    'volume' => t('Volume'),
    'mute' => t('Mute'),
    'unmute' => t('Unmute'),
    'enableCaptions' => t('Enable captions'),
    'disableCaptions' => t('Disable captions'),
    'download' => t('Download'),
    'enterFullscreen' => t('Enter fullscreen'),
    'exitFullscreen' => t('Exit fullscreen'),
    'frameTitle' => t('Player for {title}'),
    'captions' => t('Captions'),
    'settings' => t('Settings'),
    'pip' => t('Picture-in-picture'),
    'menuBack' => t('Back'),
    'speed' => t('Speed'),
    'normal' => t('Normal'),
    'quality' => t('Quality'),
    'loop' => t('Loop'),
    'start' => t('Start'),
    'end' => t('End'),
    'all' => t('All'),
    'reset' => t('Reset'),
    'disabled' => t('Disabled'),
    'enabled' => t('Enabled'),
    'advertisement' => t('Ad'),
    'qualityBadge' => [
      '2160' => t('4K'),
      '1440' => t('HD'),
      '1080' => t('HD'),
      '720' => t('HD'),
      '576' => t('SD'),
      '480' => t('SD'),
    ],
  ];
}

/**
 * Implements hook_form_alter().
 *
 * Ensure field_media_video_embed_field can not be spoiled by site builders.
 */
function degov_media_plyr_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id === 'entity_view_display_edit_form') {
    $isPlyrWidget = isset($form['fields']['field_media_video_embed_field']['plugin']['type']['#default_value'])
      && $form['fields']['field_media_video_embed_field']['plugin']['type']['#default_value'] === 'video_embed_field_plyr';
    if ($isPlyrWidget) {
      $form['fields']['field_media_video_embed_field']['settings_edit'] = [
        '#type' => 'inline_template',
        '#template' => '',
      ];
      if (isset($form['fields']['field_media_video_embed_field']['settings_summary'])) {
        $form['fields']['field_media_video_embed_field']['settings_summary'] = [
          '#type' => 'inline_template',
          '#template' => t('The module degov_media_plyr provides default configuration.'),
        ];
      }
    }
  }
}
