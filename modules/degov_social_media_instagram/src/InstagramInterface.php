<?php

namespace Drupal\degov_social_media_instagram;

/**
 * Class Instagram.
 *
 * @package Drupal\degov_social_media_instagram
 */
interface InstagramInterface {

  /**
   * @param int $count
   *
   * @return InstagramMediaItem[]
   */
  public function getMedias(int $count = 20): array;

}
