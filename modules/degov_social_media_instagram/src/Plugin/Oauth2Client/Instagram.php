<?php

declare(strict_types=1);

namespace Drupal\degov_social_media_instagram\Plugin\Oauth2Client;

use Drupal\oauth2_client\Plugin\Oauth2Client\Oauth2ClientPluginBase;
use League\OAuth2\Client\Token\AccessToken;

/**
 * OAuth2 Client to authenticate with Instagram
 *
 * @Oauth2Client(
 *   id = "degov_social_media_instagram",
 *   name = @Translation("deGov social media - Instagram feed block"),
 *   grant_type = "authorization_code",
 *   authorization_uri = "https://api.instagram.com/oauth/authorize",
 *   token_uri = "https://api.instagram.com/oauth/access_token",
 *   resource_owner_uri = "",
 *   scopes = {"user_profile,user_media"},
 *   scope_separator = ",",
 *   success_message = TRUE,
 * )
 */
class Instagram extends Oauth2ClientPluginBase {

  /**
   * {@inheritdoc}
   */
  public function storeAccessToken(AccessToken $accessToken): void {
    $this->state->set('oauth2_client_access_token-' . $this->getId(), $accessToken);
    if ($this->displaySuccessMessage()) {
      $this->messenger->addStatus($this->t('OAuth token stored.'));
    }
  }

  /**
   * {@inheritdoc}
   * @return \League\OAuth2\Client\Token\AccessToken|NULL
   */
  public function retrieveAccessToken(): ?AccessToken {
    return $this->state->get('oauth2_client_access_token-' . $this->getId());
  }

  /**
   * {@inheritdoc}
   */
  public function clearAccessToken(): void {
    $this->state->delete('oauth2_client_access_token-' . $this->getId());
  }

}
