<?php

declare(strict_types=1);

namespace Drupal\degov_social_media_instagram;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\oauth2_client\Service\Oauth2ClientService;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Instagram.
 *
 * @package Drupal\degov_social_media_instagram
 */
class Instagram implements InstagramInterface, ContainerInjectionInterface {

  const ENDPOINT = 'https://graph.instagram.com';

  /**
   * @var \GuzzleHttp\Client
   */
  private $httpClient;

  /**
   * @var \Drupal\oauth2_client\Service\Oauth2ClientService
   */
  private $oauth2Client;

  /**
   * @var string
   */
  private $token;

  public function __construct(Client $http_client, Oauth2ClientService $oauth2Client) {
    $this->httpClient = $http_client;
    $this->oauth2Client = $oauth2Client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('http_client'),
      $container->get('oauth2_client.service')
    );
  }

  public function selfTest(): bool {
    try {
      $accessToken = $this->oauth2Client->getAccessToken('degov_social_media_instagram');
    }
    catch (\Exception $exception) {
      \watchdog_exception('degov_social_media_instagram', $exception);
      return FALSE;
    }
    if (!$accessToken) {
      return FALSE;
    }
    $this->token = $accessToken->getToken();
    try {
      $response = $this->httpClient->get(self::ENDPOINT . '/me', ['query' => ['access_token' => $this->token]]);
    }
    catch (\Exception $exception) {
      \watchdog_exception('degov_social_media_instagram', $exception);
      return FALSE;
    }
    if ($response->getStatusCode() === 200) {
      $jsonData = $response->getBody()->getContents();
      try {
        $decoded = \json_decode($jsonData, TRUE, 512, \JSON_THROW_ON_ERROR);
      }
      catch (\Exception $exception) {
        \watchdog_exception('degov_social_media_instagram', $exception);
        return FALSE;
      }
      return \array_key_exists('id', $decoded) && \strlen($decoded['id']) > 0;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getMedias(int $count = 20): array {
    $works = $this->selfTest();
    if ($works === FALSE) {
      return [];
    }
    try {
      $response = $this->httpClient->get(self::ENDPOINT . '/me/media', [
        'query' => [
          'access_token' => $this->token,
          'limit' => $count,
          'fields' => 'id,media_type,caption,media_url,permalink,thumbnail_url,timestamp,username'
        ]
      ]);
    }
    catch (\Exception $exception) {
      \watchdog_exception('degov_social_media_instagram', $exception);
      return [];
    }
    if ($response->getStatusCode() === 200) {
      $jsonData = $response->getBody()->getContents();
      try {
        $decoded = \json_decode($jsonData, TRUE, 512, \JSON_THROW_ON_ERROR);
      }
      catch (\Exception $exception) {
        \watchdog_exception('degov_social_media_instagram', $exception);
        return [];
      }
      if (\array_key_exists('data', $decoded) && (\is_countable($decoded['data']) ? \count($decoded['data']) : 0) > 0) {
        return \array_map(InstagramMediaItem::class . '::create', $decoded['data']);
      }
      return [];
    }
    return [];
  }

}
