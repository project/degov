<?php

declare(strict_types=1);

namespace Drupal\Tests\degov_simplenews_references\Kernel;

use Drupal\Core\Extension\Extension;
use Drupal\KernelTests\KernelTestBase;

/**
 * Class InstallationTest.
 */
class ModuleInstallationTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'media',
    'node',
    'system',
    'text',
    'link',
    'paragraphs',
    'entity_reference_revisions',
    'taxonomy',
    'views',
    'user',
    'image',
    'lightning_core',
    'views_parity_row',
    'degov_content_types_shared_fields',
    'simplenews',
    'degov_taxonomy_term_section',
    'language',
    'degov_simplenews_references',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig([
      'simplenews',
      'degov_content_types_shared_fields',
      'degov_taxonomy_term_section',
      'degov_simplenews_references'
    ]);
  }

  /**
   * Tests that the module can be installed and is available.
   */
  public function testSetup(): void {
    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler */
    $moduleHandler = $this->container->get('module_handler');
    self::assertInstanceOf(Extension::class, $moduleHandler->getModule('degov_simplenews_references'));
  }

}
