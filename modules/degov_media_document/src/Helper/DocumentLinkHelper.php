<?php

namespace Drupal\degov_media_document\Helper;

use Drupal\file\FileInterface;

/**
 * Class PlaceholderHelper.
 *
 * @package Drupal\degov_media_document\Helper
 */
class DocumentLinkHelper {

  /**
   * Map file to font awesome icon.
   *
   * @param \Drupal\file\FileInterface $file
   *   File.
   *
   * @return string
   *   Icon class.
   */
  public static function mapFileToFaIcon(FileInterface $file): string {
    $extension = self::getExtensionForFile($file);
    switch ($extension) {
      case 'doc':
      case 'docx':
      case 'odt':
        return 'fa fa-file-word-o';

      case 'xls':
      case 'xlsx':
      case 'csv':
      case 'ods':
        return 'fa fa-file-excel-o';

      case 'ppt':
      case 'pptx':
      case 'odp':
        return 'fa fa-file-powerpoint-o';

      case 'pdf':
        return 'fa fa-file-pdf-o';

      default:
        return 'fa fa-file';
    }
  }

  /**
   * Map file to a file type label.
   */
  public static function mapFileToFileTypeLabel(FileInterface $file): string {
    $extension = self::getExtensionForFile($file);
    switch ($extension) {
      case 'doc':
        return \t('Word DOC text document');

      case 'docx':
        return \t('Word DOCX text document');

      case 'odt':
        return \t('OpenDocument text document ODT');

      case 'xls':
        return \t('Excell XLS table');

      case 'xlsx':
        return \t('Excell XLSX table');

      case 'csv':
        return \t('Character-separated values CSV table');

      case 'ods':
        return \t('OpenDocument table ODS');

      case 'ppt':
        return \t('PowerPoint PPT presentation');

      case 'pptx':
        return \t('PowerPoint PPTX presentation');

      case 'odp':
        return \t('OpenDocument presentation ODP');

      case 'pdf':
        return \t('PDF - Portable Document Format');

      default:
        return \t('Other file');
    }
  }

  /**
   * Determine what target attribute value to use for a file.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file.
   *
   * @return string
   *   The target value.
   */
  public static function getTargetAttributeForFile(FileInterface $file): string {
    $extension = self::getExtensionForFile($file);
    switch ($extension) {
      case 'pdf':
        return '_blank';

      default:
        return '';
    }
  }

  /**
   * Get the extension for a file.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file.
   *
   * @return string
   *   The file extension.
   */
  private static function getExtensionForFile(FileInterface $file): string {
    return \pathinfo($file->getFilename(), PATHINFO_EXTENSION);
  }

}
