<?php

/**
 * @file
 * Drupal hooks implementations for the degov_paragraph_media_reference module.
 */

declare(strict_types=1);

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;

/**
 * Implements hook_theme().
 */
function degov_paragraph_node_reference_theme($existing, $type, $theme, $path) {
  return [
    'paragraph__node_reference' => [
      'base hook' => 'paragraph',
      'template' => 'paragraph--node-reference',
    ],
    'paragraph__node_reference__preview' => [
      'base hook' => 'paragraph',
      'template' => 'paragraph--node-reference--preview',
    ],
  ];
}

/**
 * Implements hook_modules_installed().
 */
function degov_paragraph_node_reference_modules_installed(array $modules) {
  foreach ($modules as $moduleName) {
    if ($moduleName === 'degov_paragraph_node_reference' || strpos($moduleName, 'degov_node_') === 0) {
      _degov_node_reference_bundle_list();
    }
  }
}

/**
 * Update field reference target bundles.
 */
function _degov_node_reference_bundle_list() {
  $enabled_bundles = [];
  $node_types = NodeType::loadMultiple();
  /** @var \Drupal\node\NodeTypeInterface $node_type */
  foreach ($node_types as $node_type) {
    $enabled_bundles[$node_type->id()] = $node_type->id();
  }
  if (!empty($enabled_bundles)) {
    // Load the configuration for node_reference field storage.
    $config = \Drupal::configFactory()
      ->getEditable('field.field.paragraph.node_reference.field_node_reference_nodes');
    $settings = $config->get('settings');
    $settings['handler_settings']['target_bundles'] = $enabled_bundles;
    $config->set('settings', $settings);
    $config->save(TRUE);
  }
  else {
    \Drupal::messenger()->addWarning(t('There are no content types available'));
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function degov_paragraph_node_reference_field_widget_form_alter(&$element, FormStateInterface $form_state, $context) {
  /** @var \Drupal\Core\Field\WidgetBase $widget */
  $widget = $context['widget'];

  // Only allow view modes whose machine names begin with "teaser".
  if ($widget->getPluginId() === 'entity_reference_paragraphs' && !empty($element['subform']['field_node_reference_viewmode'])) {
    $enabled_view_modes = \Drupal::config('degov_paragraph_node_reference.settings')
      ->get('enabled_view_modes');
    $element['subform']['field_node_reference_viewmode']['widget']['#options'] = array_filter($element['subform']['field_node_reference_viewmode']['widget']['#options'], function ($key) use ($enabled_view_modes) {
      return isset($enabled_view_modes[$key]) && $enabled_view_modes[$key] === $key;
    }, ARRAY_FILTER_USE_KEY);
  }
}
