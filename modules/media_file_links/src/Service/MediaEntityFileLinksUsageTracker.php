<?php

declare(strict_types=1);

namespace Drupal\media_file_links\Service;

use Drupal\degov_media_usage\Service\MediaUsagePersistance;

/**
 * Class MediaEntityFileLinksUsageTracker
 *
 * @package Drupal\media_file_links
 */
class MediaEntityFileLinksUsageTracker extends MediaUsagePersistance {

}
