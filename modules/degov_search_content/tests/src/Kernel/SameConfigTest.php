<?php

declare(strict_types=1);

namespace Drupal\Tests\degov_search_content\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\Yaml\Yaml;

/**
 * Class SameConfigTest
 *
 * @package Drupal\Tests\degov_search_content\Kernel
 */
class SameConfigTest extends KernelTestBase {

  public function testSameConfig() {

    /** @var \Drupal\Core\Extension\ExtensionList $extension_list */
    $extension_list = $this->container->get('extension.list.module');

    $config_name = '/config/install/search_api.index.search_content.yml';
    $config_search_content = Yaml::parseFile($extension_list->getPath('degov_search_content') . $config_name);
    $config_search_content_solr = Yaml::parseFile($extension_list->getPath('degov_search_content_solr') . $config_name);

    self::assertEquals($config_search_content['processor_settings']['ignore_character'], $config_search_content_solr['processor_settings']['ignore_character'], 'Have same irgnore characters settings');
    self::assertEquals($config_search_content['processor_settings']['stopwords']['stopwords'], $config_search_content_solr['processor_settings']['stopwords']['stopwords'], 'Have same stopwords list');
  }

}
