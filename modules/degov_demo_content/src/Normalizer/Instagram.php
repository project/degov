<?php

declare(strict_types=1);

namespace Drupal\degov_demo_content\Normalizer;

use Drupal\degov_social_media_instagram\InstagramMediaItem;
use Drupal\serialization\Normalizer\NormalizerBase;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Class Instagram
 *
 * @package Drupal\degov_demo_content\Normalizer
 */
class Instagram extends NormalizerBase implements DenormalizerInterface {

  /**
   * {@inheritDoc}
   */
  protected $supportedInterfaceOrClass = InstagramMediaItem::class;

  /**
   * {@inheritDoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    /** @var \Drupal\degov_social_media_instagram\InstagramMediaItem $object */
    return $object->toArray();
  }

  /**
   * {@inheritDoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {
    if (empty($data) || (is_array($data) && count($data) < 1)) {
      return [];
    }
    foreach ($data as &$item) {
      /** @var \Drupal\degov_social_media_instagram\InstagramMediaItem $class */
      $item = $class::create($item);
    }
    return $data;
  }

}
