<?php

declare(strict_types=1);

namespace Drupal\degov_common\Tests\Unit;

use Drupal\Core\Form\FormState;
use Drupal\KernelTests\KernelTestBase;
use Drupal\user\Form\UserLoginForm;

/**
 * Class LoginFormInputFieldsAttribute
 *
 * @package Drupal\degov_common\Tests\Unit
 */
class UserLoginFormInputFieldsAttributeTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'user'
  ];

  public function testAttributesDoesNotExists() {
    $userLoginForm = UserLoginForm::create($this->container);
    $form = [];
    $form_state = new FormState();
    $builded_form = $userLoginForm->buildForm($form, $form_state);
    // When the test failed then review degov_common_form_user_login_form_alter which adds the attribute autocomplete.
    self::assertArrayNotHasKey('autocomplete', $builded_form['name']['#attributes']);
  }

}
