<?php

namespace Drupal\Tests\degov_breadcrumb\Kernel;

use Drupal\Tests\degov_common\Kernel\ModuleInstallationTestAbstract;

/**
 * Class ModuleInstallationTest.
 */
class ModuleInstallationTest extends ModuleInstallationTestAbstract {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'degov_breadcrumb',
  ];

}
