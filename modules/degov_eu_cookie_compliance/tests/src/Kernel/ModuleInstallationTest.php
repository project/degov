<?php

namespace Drupal\Tests\degov_eu_cookie_compliance\Kernel;

use Drupal\Core\Extension\Extension;
use Drupal\KernelTests\KernelTestBase;

/**
 * Class ModuleInstallationTest.
 */
class ModuleInstallationTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'config_replace',
    'eu_cookie_compliance',
    'degov_eu_cookie_compliance',
  ];

  /**
   * Test setup.
   */
  public function testInstallation(): void {
    /**
     * @var \Drupal\Core\Extension\ModuleHandler $moduleHandler
     */
    $moduleHandler = $this->container->get('module_handler');
    self::assertInstanceOf(Extension::class, $moduleHandler->getModule('degov_eu_cookie_compliance'));

    $this->installConfig(self::$modules);
    $this->container->get('config_replace.config_replacer')->rewriteModuleConfig('degov_eu_cookie_compliance');
  }

}
