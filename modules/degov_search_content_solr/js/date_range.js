/**
 * @file
 * Date range facet block functions.
 */

(function ($) {

  'use strict';

  /**
   * Initialize the Drupal facets.
   *
   * @type {{}}
   */
  Drupal.facets = Drupal.facets || {};

  /**
   * Attache the behaviour.
   *
   * @type {{attach: Drupal.behaviors.dateInputRange.attach}}
   */
  Drupal.behaviors.dateInputRange = {
    attach: function (context, settings) {
      Drupal.facets.dateInputRange(context, settings);
    }
  };

  /**
   * Behaviour for date range block with datepicker.
   *
   * @param context
   * @param settings
   */
  Drupal.facets.dateInputRange = function (context, settings) {
    // Initialize the datepicker.
    $('input[type="date"]', context).datepicker({ dateFormat: 'dd.mm.yy' }).attr('type','text');
    // Check for default values.
    if (typeof settings.dateFilter != 'undefined') {
      $('#facet_filter_date_range_picker_date_from', context).val(settings.dateFilter.min);
      $('#facet_filter_date_range_picker_date_to', context).val(settings.dateFilter.max);
    }

    $('.date-filter', context).on('click', function () {
      var dateFrom = $('#facet_filter_date_range_picker_date_from').val();
      var dateTo = $('#facet_filter_date_range_picker_date_to').val();
      // If the value is empty set to any (*).
      if (dateFrom === '') {
        dateFrom = '*';
      }
      if (dateTo === '') {
        dateTo = '*';
      }
      if (settings.dateFilter.facetUrl !== '') {
        var href = settings.dateFilter.facetUrl.replace('date_min', dateFrom);
        href = href.replace('date_max', dateTo);
        // Redirect to search page with correct query parameters.
        window.location.href = href;
      }
    });
  }

})(jQuery);
